%{
#include<stdio.h>
#include <stdlib.h>
#include <string.h>

int yywrap ( void ) { return 1; };
%}

/* Definições Regulares */
delimitador		[ \t\n]
delimitadores		{delimitador}+
digito			[0-9]
maiusculas		[A-Z]
minusculas		[a-z]
inteiropositivo		{digito}+|\+{digito}+
inteironegativo		\-{digito}+
decimal			{digito}+\.{digito}+
placa			{maiusculas}{maiusculas}{maiusculas}\-{digito}{digito}{digito}{digito}
palavra			({maiusculas}|{minusculas})+
telefone		{digito}{digito}{digito}{digito}\-{digito}{digito}{digito}{digito}
nome			{palavra}[ ]{palavra}[ ]{palavra}|{palavra}[ ]{palavra}[ ]{palavra}[ ]{palavra}

%%

{delimitadores}		{/* Apenas ignorar */}
{telefone}		{printf("Foi encontrado um telefone. LEXEMA: %s\n", yytext);}
{inteiropositivo}	{printf("Foi encontrado um numero inteiro positivo. LEXEMA: %s\n", yytext);}
{inteironegativo}	{printf("Foi encontrado um numero inteiro negativo. LEXEMA: %s\n", yytext);}
{decimal}		{printf("Foi encontrado um numero com parte decimal. LEXEMA: %s\n", yytext);}
{placa}			{printf("Foi encontrado uma placa. LEXEMA: %s\n", yytext);}
{nome}			{printf("Foi encontrado um nome proprio. LEXEMA: %s\n", yytext);}
{palavra}		{printf("Foi encontrado uma palavra. LEXEMA: %s\n", yytext);}

%%

int main(void){
   
   int tipo_entrada;
   FILE *arquivo_entrada = NULL;
   char nome_arquivo[50];

   printf("Informe o tipo de entrada a ser realizada:\n1 - Ler do terminal\n2 - Ler do arquivo\nQualquer outro numero - Sair\n");
   scanf("%d", &tipo_entrada);
   
   if(tipo_entrada == 1){
      printf("Informe o texto de entrada:\n");
      yylex();	
   }
   else if (tipo_entrada == 2){
      printf("Informe o nome do arquivo de entrada: ");
      scanf("%s", nome_arquivo);
      arquivo_entrada = fopen(nome_arquivo,"rt");
      if(arquivo_entrada  == NULL){
        printf("Erro na leitura do arquivo!!!\n");
        exit(1);
      }
      yyset_in(arquivo_entrada);
      yylex();
      fclose(arquivo_entrada);	
   }   

   return 0;

}
